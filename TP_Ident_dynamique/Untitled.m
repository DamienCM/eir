clear
load('accel.mat');
load('couple.mat');
load('vitesse.mat');
load('position.mat');

% xi = [ C1 ; I1 ]
% W = [ Op Os ]

c = c(2,:)';
thetapoint = thetapoint(2,:);
thetasec = thetasec(2,:);
w = [thetapoint ; thetasec]';
xi = pinv(w)*c;
disp('erreur C1 : ' + string(abs(0.2-xi(1))));
disp('erreur i1 : ' +string(abs(5-xi(2))));
