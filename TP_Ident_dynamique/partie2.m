clear
load('accel.mat');
load('couple.mat');
load('vitesse.mat');
load('position.mat');

% xi = [ C1 ; I1 ]
% W = [ Op Os ]

c = c(2,:)';
thetapoint = thetapoint(2,:);
thetasec = thetasec(2,:);
w = [thetapoint ; thetasec]';
xi = pinv(w)*c;
disp('erreur C1 : ' + string(abs(0.2-xi(1))));
disp('erreur i1 : ' +string(abs(5-xi(2))));

clear

XI = [];
COND = [];
ERREUR = [];
err_param = [];
alpha_th = 21.886;
beta_th = 2.886;
epsilon_th = 5.396;
nu_th = -1.000;

for traj = ["traj1.dat", "traj2.dat", "traj3.dat"]
   % Extract data
    data = load(traj);
   t = data(:,1);
   q1 = data(:,2)*pi/180;
   q2 = data(:,3)*pi/180;
   q1p =data(:,4)*pi/180;
   q2p =data(:,5)*pi/180;
   q1s =data(:,6)*pi/180;
   q2s =data(:,7)*pi/180;
   T1 =data(:,8);
   T2 =data(:,9);
   T = [T1 ; T2];
   
   Y1 = -2*sin(q2).*q1p.*q2p-sin(q2).*q2p.*q2p;
   Y2 = 2*cos(q2).*q1p.*q2p+cos(q2).*q2p.*q2p;
   Y3 = sin(q2).*q1p.*q1p;
   Y4 = -cos(q2).*q1p.*q1p;
   zero = zeros(501,1);
   
   W = [
       q1s q2s Y1+2*q1s.*cos(q2)+q2s.*cos(q2) Y2+2*sin(q2).*q1s+q2s.*sin(q2);
       zero q1s+q2s Y3+q1s.*cos(q2) q1s.*sin(q2)
       ];
   
   COND = [COND cond(W)];
   
   xi = pinv(W)* T;
   XI = [XI xi];
   
   alpha = xi(1);
   beta = xi(2);
   epsilon = xi(3);
   nu = xi(4);
   
   err_a = (alpha_th-alpha )/ alpha_th;
   err_b = (beta_th-beta)/ beta_th;
   err_e = (epsilon_th-epsilon)/ epsilon_th;
   err_n = (nu_th-nu)/ nu_th;
   err_param = [err_param [err_a err_b err_e err_n]'];
   ERREUR = [ERREUR sqrt( err_a^2 + err_b^2 + err_e^2 + err_n^2 )];
   
end
alpha_m = mean(XI(1,:));
beta_m = mean(XI(2,:));
epsilon_m = mean(XI(3,:));
nu_m = mean(XI(4,:));


