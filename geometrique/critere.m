function [crit] = critere(LHDE234)
    
% %     Extraction des donnees
    LHDE234
    L = LHDE234(1);
    H = LHDE234(2);
    D = LHDE234(3);
    E = LHDE234(4);
    off2 = LHDE234(5);    
    off3 = LHDE234(6);    
    off4 = LHDE234(7);
    % Computes the diff Qc Qm from data measured
    global Xm Qm 
    
    % Ajout des offset articulaires
    qm1 = Qm(1,:);
    qm2 = Qm(2,:)+off2;
    qm3 = Qm(3,:)+off3;
    qm4 = Qm(4,:)+off4;
    qmloc = [qm1; qm2; qm3; qm4];

    % Calcul des positions
    Qc = MGI(Xm,L,H,D,E);

    % Erreur (a minimiser par LSQNONLIN)
    crit = Qc - qmloc;
end

