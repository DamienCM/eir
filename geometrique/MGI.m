function [q] = MGI(X,L,H,D,E)
% MGI( x,y,z,theta(t) ) --> qi(t) (i=1..4)
    
    % Extraction
    x = X(1,:);
    y = X(2,:);
    z = X(3,:);
    theta = X(4,:);
    
    % Modele
    T = theta/1200;
    
    % Calculs coord articulaires
    q1a = (-E+T)+x + sqrt(-D^2 + 2*H*D + 2*D*y - H^2 - 2*H*y + L^2 - y.^2 - z.^2);
    q1b = (-E+T)+x - sqrt(-D^2 + 2*H*D + 2*D*y - H^2 - 2*H*y + L^2 - y.^2 - z.^2);

    q2a = -E+T+x + sqrt(-D^2 + 2*H*D + 2*D*y - H^2 - 2*H*y + L^2 - y.^2 - z.^2);
    q2b = -E+T+x - sqrt(-D^2 + 2*H*D + 2*D*y - H^2 - 2*H*y + L^2 - y.^2 - z.^2);

    q3a = -E +x + sqrt(-D^2 + 2*H*D - 2*D*y - H^2 + 2*H*y + L^2 - y.^2 - z.^2);
    q3b = -E +x - sqrt(-D^2 + 2*H*D - 2*D*y - H^2 + 2*H*y + L^2 - y.^2 - z.^2);

    q4a = +E+x + sqrt(-D^2 + 2*H*D - 2*D*y - H^2 + 2*H*y + L^2 - y.^2 - z.^2);
    q4b = +E+x - sqrt(-D^2 + 2*H*D - 2*D*y - H^2 + 2*H*y + L^2 - y.^2 - z.^2);

%     q1 = min([q1a;q1b],[],1);
%     q2 = max([q2a;q2b],[],1);
%     q3 = min([q3a;q3b],[],1);
%     q4 = max([q4a;q4b],[],1);

    
    % - + - + seul combo a donner un reel
    q = [q1b ; q2a ; q3b ; q4a];
    
end

