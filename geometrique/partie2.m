clear

% Chargement du jeu de donnees

load('poses.mat')


% Extraction des donnees 
global Xm Qm


% offset articulaires a determiner
offset_2 = -0.3021;
offset_3 = 0;
offset_4 = 0;

% Donnees expermintales
Qm = [q(1,:) ; q(2,:) ; q(3,:); q(4,:)];
Xm = x(:,:);
Xm = Xm(:,2:end);
Qm = Qm(:,2:end);

% Valeurs theoriques (valeurs initiales de la descente de gradient)

L=1.000;
H=0.5;
D=0.15;
E=0.15;

optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt');
options.FunctionTolerance = 1e-9;
[param_id, resnorm,residual,exitflag,output,lambda,jacobian] = lsqnonlin(@critere, [L, H, D, E, offset_2, offset_3,offset_4],[],[],options);

% [x,fval,exitflag,output] = fminsearch(@critere, [L, H, D, E, offset_2, offset_3, offset_4]);


L = param_id(1)
H = param_id(2)
D = param_id(3)
E = param_id(4)
off2 = param_id(5)
off3 = param_id(6)
off4 = param_id(7)

% Analyse des resultats
% erreurs = critere([L, H, D, E, offset_2, offset_3, offset_4]);
% std_q1 = std(erreurs(1,:));
% std_q2 = std(erreurs(2,:));
% std_q3 = std(erreurs(3,:)); 
% std_q4 = std(erreurs(4,:));
% 
% % A comparer avec la resolution du capteur
% disp("q1 precis a +- :" + string(3*std_q1))
% disp("q2 precis a +- :" + string(3*std_q2))
% disp("q3 precis a +- :" + string(3*std_q3))
% disp("q4 precis a +- :" + string(3*std_q4))

