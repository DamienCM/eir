import glob
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import os
import glob

import pandas as pd


def getFiles(path):
    complete_path = f'{path}/*.tif'
    files = glob.glob(complete_path)
    return files

def plot_points3d(csv_path):

    df = pd.read_csv(csv_path)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    x, y, z, point_label = df['Xr'], df['Yr'], df['Zr'], df['label']

    # Scatter plot 3d the points and text the labels

    for i in range(len(x)):
        if point_label[i].startswith("P0") or point_label[i].startswith("P2"):
            ax.text(x[i], y[i], z[i], point_label[i], color='r')
            ax.scatter(x[i], y[i], z[i], c='r', marker='o')
        elif point_label[i].startswith("P"):
            ax.text(x[i], y[i], z[i], point_label[i], color='orange')
            ax.scatter(x[i], y[i], z[i], c='orange', marker='x')
        elif point_label[i].startswith("C"):
            ax.text(x[i], y[i], z[i], point_label[i], color='blue')
            ax.scatter(x[i], y[i], z[i], c='blue', marker='^')
    # plot the axes
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    x0,y0,z0 = df['Xr'][0], df['Yr'][0], df['Zr'][0]

    ax.plot((-400,800),(y0,y0),(z0,z0),color='k')
    ax.plot((x0,x0),(200,1400),(z0,z0),color='k')
    ax.plot((x0,x0),(y0,y0),(-100,400),color='k')
    plt.show()
    return df


def axisEqual3D(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
    ax: a matplotlib axis, e.g., as output from plt.gca().
    '''
    # Create cubic bounding box to simulate equal aspect ratio
    
    max_range = np.array([2000, 2000, 4000]).max()
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() 
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten()
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten()+ 0.5*(3500)
    for xb, yb, zb in zip(Xb, Yb, Zb):
        ax.plot([xb], [yb], [zb], color='k')

def plot_panel(ax, rvec, tvec,n):
    """plot a single panel 

    Args:
        ax (mpl ax): aax to plot
        rvec (array 3x1): rotation vector
        tvec (array 3x1): translation vector   
        n (any): label
    """    
    mat = cv.Rodrigues(rvec.transpose())[0]
    x1, x2, x3 = np.dot(mat, np.transpose(np.array([1, 0, 0])))
    y1, y2, y3 = np.dot(mat, np.transpose(np.array([0, 1, 0])))
    z1, z2, z3 = np.dot(mat, np.transpose(np.array([0, 0, 1])))

    # Reperes
    plt.quiver(tvec[0], tvec[1], tvec[2], x1, x2, x3, color='r', length=100)
    plt.quiver(tvec[0], tvec[1], tvec[2], y1, y2, y3, color='g', length=100)
    plt.quiver(tvec[0], tvec[1], tvec[2], z1, z2, z3, color='b', length=200)

    # Grids
    surfx = []
    surfy = []
    surfz = []
    for i in range(11):
        for j in range(11):
            x = 30*i
            y = 30*j
            surfx.append(tvec[0][0]+x*x1+y*y1)
            surfy.append(tvec[1][0]+x*x2+y*y2)
            surfz.append(tvec[2][0]+x*x3+y*y3)
    surfx = np.array(surfx)
    surfy = np.array(surfy)
    surfz = np.array(surfz)
    ax.plot(surfx,surfy,surfz, label=n+1)


def calibration(files, n, D, verbose = False):
    """calibration for a set of files

    Args:
        files (list): list files
        n (int): no of squares along x,y
        D (float): distance between squares
        verbose (bool): print or not
    """    
    # termination criteria
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((n*n, 3), np.float32)
    objp[:, :2] = np.mgrid[0:n, 0:n].T.reshape(-1, 2)*D
    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    for fname in files:
        if verbose :
            print(f'Reading : {fname}')
        img = cv.imread(fname)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, (n, n), None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv.cornerSubPix(gray, corners, (n, n), (-1, -1), criteria)
            imgpoints.append(corners2)
            
            if verbose:
                cv.drawChessboardCorners(img, (n,n), corners2, ret)
                # Draw and display the corners
                cv.imshow('img', img)
                cv.waitKey(0)

    calib_data = cv.calibrateCameraExtended(objpoints, imgpoints, gray.shape[::-1],None, None)
    
    retval, cameraMatrix, distCoeffs, rvecs, tvecs, stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors = calib_data
    calib_data = {
        "retval" : retval,
        "cameraMatrix" : cameraMatrix,
        "distCoeffs" : distCoeffs,
        "rvecs" : rvecs,
        "tvecs" : tvecs, 
        "stdDeviationsIntrinsics" : stdDeviationsIntrinsics,
        "stdDeviationsExtrinsics" : stdDeviationsExtrinsics,
        "perViewErrors" : perViewErrors,
        "objpoints" : objpoints,
        "imgpoints" : imgpoints,
        "objp" : objp,
    }

    if verbose :
        print(f" Camera Matrix : \n {calib_data['cameraMatrix']}\n")
        print(f"Distortion Coeffs : {calib_data['distCoeffs']}")

    return calib_data
    



def plot_grids(objpoints, mtx, dist, rvecs, tvecs):
    """plot all the grids 

    Args:
        objpoints ([type]): [description]
        mtx ([type]): [description]
        dist ([type]): [description]
        rvecs ([type]): [description]
        tvecs ([type]): [description]
    """    
    # 3d plot
    fig = plt.figure(figsize=(10,10))
    ax = plt.axes(projection='3d')
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        plot_panel(ax, rvecs[i], tvecs[i],i)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_zlim3d(0, 3500)
    axisEqual3D(ax)
    ax.scatter(0,0,0,s=50, c='k',label='camera')
    fig.legend()
    plt.show()



if __name__ == "__main__" :
    path = '.'
    files = getFiles(path)

    n = 11
    D = 20
    calibration_data = calibration(files, n, D, verbose=False)
    
    objpoints = calibration_data["objpoints"]
    mtx = calibration_data["cameraMatrix"]
    dist =calibration_data["distCoeffs"]
    rvecs = calibration_data["rvecs"]
    tvecs = calibration_data["tvecs"]
    objp = calibration_data["objp"]
    print(f"Camera Matrix : \n {mtx}\n")
    print(f"Distortion Coeffs : {dist}")
    print(f"Calibration Retroprojection error : ", (calibration_data["perViewErrors"]))
    print(f"Calibration Error : ", calibration_data["stdDeviationsIntrinsics"])
    # plot_grids(objpoints,mtx, dist, rvecs, tvecs)

    repet_files = glob.glob('../justesse_fidelite/*I*.tif')
        
    P0 = []
    P2 = []

    for f in repet_files :
        if f.__contains__('P00'):
            P0.append(f)
        else:
            P2.append(f)    

    P0 = sorted(P0)
    P2 = sorted(P2)

    P0T = []

    for p0 in P0 :
        img = cv.imread(p0)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        ret, corners = cv.findChessboardCorners(gray, (n, n), None)
        # estimate the pose of the camera
        retval, rvec, tvec = cv.solvePnP(objp, corners, mtx, dist)
        P0T.append(tvec)
    P0T = np.array(P0T)
    print("------------------")
    print("Std dev of P0T x : ", np.std(P0T[:,0]))
    print("Std dev of P0T y : ", np.std(P0T[:,1]))
    print("Std dev of P0T z : ", np.std(P0T[:,2]))
    print("Mean of P0T x : ", np.mean(P0T[:,0]))
    print("Mean of P0T y : ", np.mean(P0T[:,1]))
    print("Mean of P0T z : ", np.mean(P0T[:,2]))
    p0xi = np.mean(P0T[:,0])
    p0yi = np.mean(P0T[:,1])
    p0zi = np.mean(P0T[:,2])

    P2T = []
    for p2 in P2 :
        img = cv.imread(p2)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        ret, corners = cv.findChessboardCorners(gray, (n, n), None)
        # estimate the pose of the camera
        retval, rvec, tvec = cv.solvePnP(objp, corners, mtx, dist)
        P2T.append(tvec)
    P2T = np.array(P2T)
    print("------------------")
    print("Std dev of P2T x : ", np.std(P2T[:,0]))
    print("Std dev of P2T y : ", np.std(P2T[:,1]))
    print("Std dev of P2T z : ", np.std(P2T[:,2]))
    print("Mean of P2T x : ", np.mean(P2T[:,0]))
    print("Mean of P2T y : ", np.mean(P2T[:,1]))
    print("Mean of P2T z : ", np.mean(P2T[:,2]))

    print("------------------")
    PT = P0T - P2T
    xt = np.mean(PT[:,0])
    yt = np.mean(PT[:,1])
    zt = np.mean(PT[:,2])
    print("Norm of P0T - P2T x : ", np.linalg.norm([xt,yt,zt]))
    
    print("------------------")
    df = pd.read_csv('coord.csv')
    Xr, Yr, Zr = df['Xr'], df['Yr'], df['Zr']
    p0x, p0y, p0z =Xr[0], Yr[0], Zr[0]
    
    precision_files = glob.glob('../justesse_fidelite/*.tif')

    files = []

    for f in precision_files :
        if not f.__contains__('I'):
            files.append(f) 
    
    xs, ys, zs, ns = [], [], [] ,[]

    for f in files :
        fname = f.split('/')[-1][:-4]
        img = cv.imread(f)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        ret, corners = cv.findChessboardCorners(gray, (n, n), None)
        # estimate the pose of the camera
        retval, rvec, tvec = cv.solvePnP(objp, corners, mtx, dist)
        x, y, z = tvec[0][0], tvec[1][0], tvec[2][0]
        # Extract corresponding row from coord.csv 
        row = df[df['label'] == fname]
        Xr, Yr, Zr = row['Xr'].values[-1], row['Yr'].values[-1], row['Zr'].values[-1]
        xs.append(Xr)
        ys.append(Yr)
        zs.append(Zr)


        # print("Rob coord ", Xr, Yr, Zr)
        # print("Rob 0 :", p0x, p0y, p0z)
        # print("Image coord ", x, y, z)
        # print("Image 0 :", p0xi, p0yi, p0zi)
        # print("Robot diff :", Xr - p0x, Yr - p0y, Zr - p0z)
        # print("Image diff :", x - p0xi, y - p0yi, z - p0zi)
        # Evaluate the norm of the difference between the estimated and the real position
        norm_r = np.linalg.norm([p0x-Xr, p0y-Yr, p0z-Zr])
        # print("Norm Robot : ", norm_r)
        norm_i = np.linalg.norm([p0xi-x, p0yi-y, p0zi-z])
        # print("Norm Image : ", norm_i)
        norm_diff = np.linalg.norm(norm_r - norm_i)
        ns.append(norm_diff)
        print(fname, norm_diff)
    

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    # PLot main axis
    ax.plot([p0x, p0x], [250, 1500], [p0z, p0z], '-', color='black')
    ax.plot([-300, 800], [p0y, p0y], [p0z, p0z], '-', color='black')
    ax.plot([p0x, p0x], [p0y, p0y], [-200, 500], '-', color='black')
    cmap = ax.scatter(xs, ys, zs, c=ns, s=200, marker='o')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    cbar = fig.colorbar(cmap)
    cbar.set_label('Erreur')
    plt.show()