#!/bin/bash

apt-get install libxml2-dev libssl-dev libffi-dev libxslt1-dev python-dev libjpeg-dev
pip freeze --local >pipfreeze.txt
tr '\n' ' ' < pipfreeze.txt >pipfreeze2.txt
pip install --upgrade --force-reinstall `cat pipfreeze2.txt`