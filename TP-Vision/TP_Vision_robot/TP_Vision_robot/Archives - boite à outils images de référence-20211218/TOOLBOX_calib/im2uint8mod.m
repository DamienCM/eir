function u = im2uint8(img, typestr)
if isa(img, 'uint8')
    u = img; 
   
elseif isa(img, 'double') | isa(img, 'uint16')
    if nargin==1
        if islogical(img)      
            % binary image
            u = uint8(img~=0);
        else
            %u = grayto8(img);
            u=uint8( double(img)*(1/257.0)+0.5 );
            %%
        end
        
    elseif nargin==2
    
        if ~ischar(typestr) | (typestr(1) ~= 'i')
            error('Invalid input arguments');
        else 
            if (isa(img, 'uint16'))
                if (max(img(:)) > 255)
                    error('Too many colors for 8-bit integer storage.');
                else
                    u = uint8(img);
                end
                
            else
                % img is double
                if max(img(:))>=257 
                    error('Too many colors for 8-bit integer storage.');
                elseif min(img(:))<1
                    error('Invalid indexed image: an index was less than 1.');
                else
                    u = uint8(img-1);
                end
            end
        end
    else
        error('Invalid input arguments.');
    end
else
    error('Unsupported input class.');
end
