% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 2684.140355673120212 ; 2683.675307659709688 ];

%-- Principal point:
cc = [ 920.901866908768397 ; 579.160013055744230 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.184044348302785 ; 0.892953077127457 ; -0.001961179067234 ; -0.004790899007390 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 47.641191114735072 ; 45.973720280118300 ];

%-- Principal point uncertainty:
cc_error = [ 45.393902418127603 ; 48.215289767678904 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.040672417397610 ; 0.300214406897027 ; 0.005256098672353 ; 0.005502227857699 ; 0.000000000000000 ];

%-- Image size:
nx = 1920;
ny = 1200;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 16;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.038184e+00 ; -2.033731e+00 ; -2.476467e-01 ];
Tc_1  = [ -2.998459e+03 ; -1.613113e+03 ; 9.688290e+03 ];
omc_error_1 = [ 1.574294e-02 ; 1.353796e-02 ; 2.823820e-02 ];
Tc_error_1  = [ 1.689503e+02 ; 1.829706e+02 ; 1.994449e+02 ];

%-- Image #2:
omc_2 = [ -1.558248e+00 ; -2.233272e+00 ; -8.822857e-01 ];
Tc_2  = [ -2.306227e+03 ; -1.270598e+03 ; 9.480947e+03 ];
omc_error_2 = [ 9.614815e-03 ; 1.722022e-02 ; 2.273672e-02 ];
Tc_error_2  = [ 1.620948e+02 ; 1.772232e+02 ; 1.944436e+02 ];

%-- Image #3:
omc_3 = [ 9.082396e-01 ; 2.579349e+00 ; -1.032884e+00 ];
Tc_3  = [ -1.760667e+03 ; -1.231990e+03 ; 1.050898e+04 ];
omc_error_3 = [ 6.278642e-03 ; 2.281490e-02 ; 2.596316e-02 ];
Tc_error_3  = [ 1.789933e+02 ; 1.934584e+02 ; 1.848094e+02 ];

%-- Image #4:
omc_4 = [ -2.085893e+00 ; -2.033951e+00 ; -5.421024e-01 ];
Tc_4  = [ 8.011488e+02 ; -1.401266e+03 ; 9.186292e+03 ];
omc_error_4 = [ 1.129627e-02 ; 1.866316e-02 ; 2.814040e-02 ];
Tc_error_4  = [ 1.562700e+02 ; 1.663050e+02 ; 1.818726e+02 ];

%-- Image #5:
omc_5 = [ -1.615668e+00 ; -2.135474e+00 ; 7.229246e-01 ];
Tc_5  = [ 1.438440e+03 ; 5.163820e+02 ; 1.129025e+04 ];
omc_error_5 = [ 1.313099e-02 ; 1.243792e-02 ; 2.328900e-02 ];
Tc_error_5  = [ 1.939884e+02 ; 2.044484e+02 ; 1.896933e+02 ];

%-- Image #6:
omc_6 = [ 2.082121e+00 ; 2.002579e+00 ; 4.214257e-01 ];
Tc_6  = [ -7.939854e+02 ; 6.912580e+02 ; 1.090267e+04 ];
omc_error_6 = [ 1.847414e-02 ; 1.183130e-02 ; 3.058333e-02 ];
Tc_error_6  = [ 1.842229e+02 ; 1.956608e+02 ; 1.938117e+02 ];

%-- Image #7:
omc_7 = [ -2.251569e+00 ; -2.116554e+00 ; -2.313584e-01 ];
Tc_7  = [ -1.331375e+03 ; -2.674389e+02 ; 1.002001e+04 ];
omc_error_7 = [ 1.910586e-02 ; 1.875612e-02 ; 3.822247e-02 ];
Tc_error_7  = [ 1.691177e+02 ; 1.810421e+02 ; 1.860594e+02 ];

%-- Image #8:
omc_8 = [ -2.287350e+00 ; -2.080338e+00 ; 1.920264e-01 ];
Tc_8  = [ -1.019569e+03 ; -3.302717e+02 ; 6.484117e+03 ];
omc_error_8 = [ 2.003346e-02 ; 1.582212e-02 ; 2.952851e-02 ];
Tc_error_8  = [ 1.098406e+02 ; 1.171358e+02 ; 1.129079e+02 ];

%-- Image #9:
omc_9 = [ 1.828469e+00 ; 2.072787e+00 ; 1.404760e+00 ];
Tc_9  = [ -2.014255e+03 ; 5.692981e+02 ; 7.087841e+03 ];
omc_error_9 = [ 2.146451e-02 ; 8.116517e-03 ; 2.453047e-02 ];
Tc_error_9  = [ 1.212151e+02 ; 1.300930e+02 ; 1.472847e+02 ];

%-- Image #10:
omc_10 = [ -8.991948e-01 ; -2.711993e+00 ; -3.503626e-01 ];
Tc_10  = [ 1.258142e+03 ; -1.664747e+03 ; 8.055820e+03 ];
omc_error_10 = [ 5.282396e-03 ; 1.822605e-02 ; 2.691373e-02 ];
Tc_error_10  = [ 1.379886e+02 ; 1.504492e+02 ; 1.600902e+02 ];

%-- Image #11:
omc_11 = [ 1.776432e+00 ; 1.901364e+00 ; 7.345916e-01 ];
Tc_11  = [ -1.141458e+03 ; -5.707747e+02 ; 7.098328e+03 ];
omc_error_11 = [ 1.558982e-02 ; 1.084093e-02 ; 2.239976e-02 ];
Tc_error_11  = [ 1.207381e+02 ; 1.287528e+02 ; 1.295392e+02 ];

%-- Image #12:
omc_12 = [ -1.815403e+00 ; -1.619690e+00 ; 6.260638e-01 ];
Tc_12  = [ 1.885951e+01 ; -5.804684e+02 ; 8.268025e+03 ];
omc_error_12 = [ 1.643024e-02 ; 1.067669e-02 ; 1.869446e-02 ];
Tc_error_12  = [ 1.397833e+02 ; 1.486220e+02 ; 1.345186e+02 ];

%-- Image #13:
omc_13 = [ 1.956322e+00 ; 2.081724e+00 ; 9.235271e-01 ];
Tc_13  = [ 4.477556e+02 ; -5.483760e+02 ; 7.405602e+03 ];
omc_error_13 = [ 1.960829e-02 ; 1.169979e-02 ; 2.682834e-02 ];
Tc_error_13  = [ 1.254212e+02 ; 1.331133e+02 ; 1.390818e+02 ];

%-- Image #14:
omc_14 = [ 2.424885e+00 ; 1.556225e+00 ; 3.800619e-01 ];
Tc_14  = [ -2.169048e+03 ; 9.002562e+02 ; 8.943850e+03 ];
omc_error_14 = [ 2.033796e-02 ; 1.112098e-02 ; 3.435310e-02 ];
Tc_error_14  = [ 1.534069e+02 ; 1.631509e+02 ; 1.670167e+02 ];

%-- Image #15:
omc_15 = [ -1.829368e+00 ; -1.586964e+00 ; 9.131254e-02 ];
Tc_15  = [ -2.223780e+03 ; -4.077428e+02 ; 8.535841e+03 ];
omc_error_15 = [ 1.774211e-02 ; 1.310322e-02 ; 1.950816e-02 ];
Tc_error_15  = [ 1.464368e+02 ; 1.566840e+02 ; 1.577150e+02 ];

%-- Image #16:
omc_16 = [ -1.945370e+00 ; -1.888103e+00 ; 1.749604e-01 ];
Tc_16  = [ 1.256517e+03 ; 7.024066e+02 ; 1.086661e+04 ];
omc_error_16 = [ 1.289734e-02 ; 1.544858e-02 ; 2.583555e-02 ];
Tc_error_16  = [ 1.853992e+02 ; 1.964960e+02 ; 1.987287e+02 ];

